import os
from re import L
import time
from app import App
from stmpy import Machine, Driver
import stmpy
import signal


class ClientStateMachine:
    """
    Client State Machine.
    """

    def __init__(self):
        self.app = None
        self.stm = None

    def show_login_screen(self):
        """
        Display login screen.
        """

        if self.app:
            self.app.hide_frame()
            self.app.show_frame("Login", show=True)

    def display_available_rooms(self):
        """
        Display Room menu
        """
        self.app.show_frame("RoomMenu", show=True)

    def stop(self):
        """
        Stop the state machine.
        """

        process = os.getpgid(0)
        os.killpg(process, signal.SIGTERM)
        time.sleep(3)
        os.killpg(process, signal.SIGKILL)

    def show_error_message(self):
        """
        Shows error message
        """

        self.app.show_error_message()
        self.app.show_frame("RoomMenu", show=True)

    def send_signal(self):
        """
        Send signal to connect to room.
        """
        self.app.hide_frame()
        self.app.show_frame("JoiningRoom", show=True)
        self.app.sleep()
        self.app.connect_to_room()

    def display_room(self):
        """
        Display the room when the user is connected to it.
        """

        self.app.hide_frame()
        self.app.show_frame("GUI")
        self.app.run_videochat()

    def unmute(self):
        """
        Turn on microphone
        """

        self.app.turn_on_audio()

    def camera_on(self):
        """
        Turn on camera
        """

        self.app.turn_on_video()

    def timed_out_message(self):
        """
        Timer timed out.
        Show Room Menu.
        """
        self.app.show_frame("RoomMenu", show=True)

    def toggle_camera(self):
        """
        Toggle camera on or off.
        """
        self.app.toggle_video()

    def toggle_mic(self):
        """
        Toggle microphone on or off.
        """

        self.app.toggle_audio()

    def leave_room(self):
        """
        Disconnect and leave room.
        """

        self.app.disconnect()
        time.sleep(2)
        self.app.show_frame("RoomMenu", show=True)


if __name__ == '__main__':
    print("STMPY Version installed: {}".format(stmpy.__version__))

    t0 = {'source': 'initial',
          'target': 'authentication',
          'effect': 'show_login_screen'}

    t1 = {'source': 'authentication',
          'target': 'end_state',
          'trigger': 'off',
          'effect': 'stop'
          }

    t2 = {'source': 'authentication',
          'target': 'authentication',
          'trigger': 'login_failed',
          'effect': 'show_error_message'}

    t3 = {'source': 'authentication',
          'target': 'room_menu',
          'trigger': 'authenticated',
          'effect': 'display_available_rooms'}

    t4 = {'source': 'room_menu',
          'target': 'authentication',
          'trigger': 'logout',
          'effect': 'show_login_screen'}

    t5 = {'source': 'room_menu',
          'target': 'joining_room',
          'trigger': 'room_selected',
          'effect': 'start_timer("t1", 8000); send_signal'}

    t6 = {'source': 'room_menu',
          'target': 'end_state',
          'trigger': 'off',
          'effect': 'stop'}

    t7 = {'source': 'joining_room',
          'target': 'room_menu',
          'trigger': 't1',
          'effect': 'timed_out_message'}

    t8 = {'source': 'joining_room',
          'target': 'room_menu',
          'trigger': 'connection_failed',
          'effect': 'show_error_message; stop_timer("t1")'}

    t9 = {'source': 'joining_room',
          'target': 'in_room',
          'trigger': 'connection_success',
          'effect': 'unmute; camera_on; display_room, stop_timer("t1")'}

    t10 = {'source': 'in_room',
           'target': 'room_menu',
           'trigger': 'leaving_room',
           'effect': 'leave_room'}

    t11 = {'source': 'in_room',
           'target': 'in_room',
           'trigger': 'press_camera',
           'effect': 'toggle_camera'}

    t12 = {'source': 'in_room',
           'target': 'in_room',
           'trigger': 'press_mute',
           'effect': 'toggle_mic'}

    t13 = {'source': 'in_room',
           'target': 'end_state',
           'trigger': 'off'}

    t14 = {
        'source': 'in_room',
        'target': 'in_room',
        'trigger': 't1',
    }

    t15 = {
        'source': 'room_menu',
        'target': 'room_menu',
        'trigger': 't1',
    }

    authentication = {
        'name': 'authentication'
    }

    room_menu = {
        'name': 'room_menu'
    }

    joining_room = {
        'name': 'joining_room'
    }

    in_room = {
        'name': 'in_room'
    }

    end_state = {
        'name': 'end_state'
    }

    client = ClientStateMachine()
    machine = Machine(name='machine', transitions=[t0, t1, t2, t3, t4, t5, t6, t7, t8, t9, t10, t11, t12, t13, t14, t15], obj=client, states=[
                      authentication, room_menu, joining_room, in_room, end_state])
    client.stm = machine

    driver = Driver()
    driver.add_machine(machine)
    driver.start()

    app = App(machine)
    client.app = app
    client.app.resizable(False, False)
    client.app.geometry("1512x750")
    client.app.mainloop()
