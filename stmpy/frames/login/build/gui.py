# This file was generated by the Tkinter Designer by Parth Jadhav
# https://github.com/ParthJadhav/Tkinter-Designer


from curses import window
from pathlib import Path

# from tkinter import *
# Explicit imports to satisfy Flake8
from tkinter import Tk, Canvas, Entry, Text, Button, PhotoImage
from tkinter import *
import tkinter as tk
import json
from turtle import color

import os
import signal
import time

OUTPUT_PATH = Path(__file__).parent
ASSETS_PATH = OUTPUT_PATH / Path("./assets")
DATA_PATH = OUTPUT_PATH / Path("./data")


def relative_to_assets(path: str) -> Path:
    return ASSETS_PATH / Path(path)


def relative_to_data(path: str) -> Path:
    return DATA_PATH / Path(path)


class Login(tk.Frame):
    def __init__(self, parent, controller, stm):
        tk.Frame.__init__(self, parent)
        self.stm = stm
        self.controller = controller

        self.canvas = Canvas(
            controller,
            bg="#FFFFFF",
            height=750,
            width=1512,
            bd=0,
            highlightthickness=0,
            relief="ridge"
        )

        self.canvas.place(x=0, y=0)

        self.button_image_1 = PhotoImage(
            file=relative_to_assets("button_1.png"))
        self.button_1 = Button(
            self.canvas,
            image=self.button_image_1,
            borderwidth=0,
            highlightthickness=0,
            command=lambda: self.login(),
            relief="flat"
        )
        self.button_1.place(
            x=1064.0,
            y=474.0,
            width=300.0,
            height=45.0
        )

        self.button_2 = Button(
            self.canvas,
            text="Quit application",
            borderwidth=0,
            highlightthickness=0,
            command=lambda: self.stm.send("off"),
            relief="flat"
        )
        self.button_2.place(
            x=1064.0,
            y=574.0,
            width=300.0,
            height=45.0
        )

        self.image_image_1 = PhotoImage(
            file=relative_to_assets("image_1.png"))
        self.image_1 = self.canvas.create_image(
            457.0,
            375.0,
            image=self.image_image_1
        )

        self.entry_image_1 = PhotoImage(
            file=relative_to_assets("entry_1.png"))
        self.entry_bg_1 = self.canvas.create_image(
            1214.0,
            394.5,
            image=self.entry_image_1
        )
        self.entry_1 = Entry(
            self.canvas,
            bd=0,
            bg="#224957",
            highlightthickness=0,
            show="*"

        )
        self.entry_1.place(
            x=1074.0,
            y=372.0,
            width=280.0,
            height=43.0
        )

        self.canvas.create_text(
            1069.0,
            346.0,
            anchor="nw",
            text="Password",
            fill="#000000",
            font=("PTSans Regular", 14 * -1)
        )

        self.entry_image_2 = PhotoImage(
            file=relative_to_assets("entry_2.png"))
        self.entry_bg_2 = self.canvas.create_image(
            1214.0,
            310.5,
            image=self.entry_image_2
        )
        self.entry_2 = Entry(
            self.canvas,
            bd=0,
            bg="#224957",
            highlightthickness=0,
        )
        self.entry_2.place(
            x=1074.0,
            y=288.0,
            width=280.0,
            height=43.0
        )

        self.canvas.create_rectangle(
            45.0,
            159.0,
            373.0,
            172.0,
            fill="#224957",
            outline="")

        self.canvas.create_text(
            1067.0,
            260.0,
            anchor="nw",
            text="Username",
            fill="#000000",
            font=("PTSans Regular", 14 * -1)
        )

        self.canvas.create_text(
            1111.0,
            212.0,
            anchor="nw",
            text="Sign in and start chitchatting!",
            fill="#000000",
            font=("PTSans Regular", 16 * -1)
        )

        self.canvas.create_text(
            1119.0,
            104.0,
            anchor="nw",
            text="Sign in",
            fill="#000000",
            font=("PTSans Regular", 64 * -1)
        )

        self.canvas.create_text(
            35.0,
            44.0,
            anchor="nw",
            text="Welcome to CoffeeChat",
            fill="#000000",
            font=("PTSans Regular", 84 * -1)
        )

        self.canvas.create_text(
            45.0,
            284.0,
            anchor="nw",
            text="This is the portal for your casual chats with your\ncolleagues. Work-related or not, discuss everything\nhere, just enjoy each other’s company. But don’t\nforget your coffee! You gotta have your daily intake of\ncaffeine :)",
            fill="#000000",
            font=("PTSans Regular", 36 * -1)
        )

        self.image_image_2 = PhotoImage(
            file=relative_to_assets("image_2.png"))
        self.image_2 = self.canvas.create_image(
            756.0,
            694.0,
            image=self.image_image_2
        )

    def show_error_message(self):
        """
        Show error message if authentication fails.
        """
        self.canvas.create_text(
            1069.0,
            426.0,
            anchor="nw",
            text="Login Failed",
            fill="#FF0000",
            font=("PTSans Regular", 14 * -1)
        )

    def login(self):
        """
        Login method to verify the user credentials.
        Reads from JSON file and checks if the user credentials are correct.
        Sends message to state machine if the user is authenticated or not.
        """
        uname = self.entry_2.get()
        pwd = self.entry_1.get()
        print(uname, pwd)
        f = open(relative_to_data("users.json"), "r")

        data = json.load(f)

        user = None
        for usr in data['users']:
            if uname == usr['username']:
                user = usr

        if user:
            if uname == user["username"] and pwd == user["password"]:
                self.controller.username = uname
                self.stm.send('authenticated')
                self.canvas.place_forget()

            else:
                self.stm.send('login_failed')

        else:
            self.stm.send('login_failed')

    def show_frame(self):
        self.canvas.place(x=0, y=0)
