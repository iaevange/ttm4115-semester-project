from threading import Thread
from turtle import bgcolor
import paho.mqtt.client as mqtt
import cv2
import base64
import time
import pyaudio
import numpy as np
import tkinter as tk
from PIL import ImageTk, Image
import sys
from array import array
from pathlib import Path


OUTPUT_PATH = Path(__file__).parent
IMAGES_PATH = OUTPUT_PATH / Path("./images")


def relative_to_images(path: str) -> Path:
    return IMAGES_PATH / Path(path)


# MQTT settings
broker, port = "mqtt.item.ntnu.no", 1883

# Audio recording settings
chunk = 1024  # record in chunks of 1024 samples
sample_format = pyaudio.paInt16  # 16 bits per sample
channels = 1
fs = 44100  # record at 44100 samples per second

# Video recording settings
frame_rate = 6  # frames per second
px_width = 320  # pixel width resolution
px_height = 180  # pixel height resolution

# FINN NAVNET DITT HER OG BRUK DET NEDERST
# List of registered users. The system supports two office locations and six remote users
users = ["trondheim", "oslo", "sebastian",
         "hallvard", "lise", "silje", "mathias", "ian"]

# Placeholder images for all of the users
# placeholder image is black
black_cv2 = cv2.cvtColor(
    np.zeros((300, 400,  3), np.uint8), cv2.COLOR_BGR2RGBA)
black_image = Image.fromarray(cv2.flip(black_cv2, 1))
images = {}  # dictionary for storing the current image of each user. After receiving video, this is updated
for user in users:
    images[user] = black_image  # first image is a placeholder black image

# image to show when camera is turned off
video_off_img = cv2.imread(
    str(relative_to_images('video_off_image.png')), cv2.COLOR_BGR2RGBA)  # read image from file
video_off_image = Image.fromarray(video_off_img)  # convert to PIL Image

# Booleans for toggle buttons
send_vid = True  # video
send_aud = True  # audio


class VideoChat:
    """
    Component to connect to the broker, send audio and video, and receive audio and video
    from all of the other users. This object plays the audio, but you need to make a GUI object
    to show the video feeds.
    """

    def __init__(self, username, stm, controller):
        self.controller = controller
        self.stm = stm
        self.username = username
        self.p = pyaudio.PyAudio()  # Create an interface to PortAudio
        self.output_stream = self.p.open(format=sample_format,  # create the audio output stream for playing audio
                                         channels=channels,
                                         rate=fs,
                                         frames_per_buffer=chunk,
                                         output=True)  # 'True' here means that the audio stream is meant for output, and not input

    def on_connect(self, client, userdata, flags, rc):
        """
        What to do when successful connection to broker happens.
        """

        print("Connected with result code {}".format(rc))
        self.stm.send("connection_success")

        print("on_connect(): {}".format(mqtt.connack_string(rc)))

    def turn_on_video(self):
        """
        Method to turn on video.
        """

        global send_vid
        send_vid = True

    def turn_on_audio(self):
        """
        Method to turn on audio.
        """

        global send_aud
        send_aud = True

    def on_message(self, client, userdata, msg):
        """
        What to do every time something is published to a topic we subscribe to.
        """

        # uncomment this if you want to see every time a new message is received
        # print("on_message(): topic: {}".format(msg.topic))

        # loop through every user, and check if we received something from them
        for user in users:
            # video
            if msg.topic == f"group5/{self.controller.room_name}/video/{user}":
                # if they have video turned off, show video off image
                if msg.payload.decode("utf-8") == "video off":
                    # add the image to dictionary
                    images[user] = video_off_image
                # if they have video turned on, add the user's new image to the image dictionary
                else:
                    # convert the bytestring back to a cv2 image
                    image = base64.b64decode(msg.payload + b'==')
                    img = np.frombuffer(image, dtype=np.uint8)
                    frame = cv2.imdecode(img, 1)
                    cv2_image = cv2.cvtColor(frame, cv2.COLOR_BGR2RGBA)
                    # flip the image horizontally
                    flipped_image = cv2.flip(cv2_image, 1)
                    # convert from cv2 image to PIL Image
                    image_ = Image.fromarray(flipped_image)
                    images[user] = image_  # add the image to the dictionary
            # audio
            if msg.topic == f"group5/{self.controller.room_name}/audio/{user}":
                # convert from bytestring to readable audio
                audio = base64.b64decode(msg.payload + b'==')
                self.play_audio(audio)  # play the audio

    def start(self, broker, port):
        """
        Method to start the connection to MQTT Broker. Also defines which topics to subcribe to. 
        Starts a thread for the mqtt client.
        """

        self.client = mqtt.Client()
        self.client.on_connect = self.on_connect
        self.client.on_message = self.on_message
        print("Connecting to {}:{}".format(broker, port))

        # connect to the specified broker and port
        self.client.connect(broker, port)

        # list with every user except yourself
        user_list = [i for i in users if i != self.username]
        for name in user_list:
            # subscribe to every other user audio topic under "group5"
            self.client.subscribe(
                f"group5/{self.controller.room_name}/audio/{name}")

        # subscribe to every video topic under "group5"
        self.client.subscribe(f"group5/{self.controller.room_name}/video/#")

        try:
            thread = Thread(target=self.client.loop_forever)
            thread.start()
        except KeyboardInterrupt:
            print("Interrupted")
            self.client.disconnect()

    def disconnect(self):
        """
        Method to disconnect from the broker.
        """

        self.client.disconnect()
        self.cap.release()
        self.input_stream.close()
        self.output_stream.close()

    def send_video(self):
        """
        Send video stream from webcam to the user's topic. 
        """

        # capture from webcam. '0' means built-in webcam
        self.cap = cv2.VideoCapture(0)
        ret = True
        prev = 0
        try:
            # while the capture is running correctly (ret == True), capture and send video
            while ret:
                time_elapsed = time.time() - prev
                ret, frame = self.cap.read()  # read the current frame from the video capture
                resized = cv2.resize(frame, (px_width, px_height), fx=0, fy=0,
                                     interpolation=cv2.INTER_CUBIC)  # change the resolution
                if time_elapsed > 1./frame_rate:  # only send 'frame_rate' frames per second
                    prev = time.time()
                    # convert the frame to sendable bytestream
                    _, buffer = cv2.imencode('.jpg', resized)
                    converted = base64.b64encode(buffer)
                    if send_vid:
                        # sending the captured frame
                        self.client.publish(
                            f"group5/{self.controller.room_name}/video/{self.username}", converted)
                    else:
                        # not sending captured frame
                        self.client.publish(
                            f"group5/{self.controller.room_name}/video/{self.username}", "video off")

        except Exception as e:
            self.cap.release()  # turn off video capture
            print(e)
        self.cap.release()

    def send_audio(self):
        """
        Send audio stream from mic to user's topic.
        """

        # open the input stream to record audio
        self.input_stream = self.p.open(format=sample_format,
                                        channels=channels,
                                        rate=fs,
                                        frames_per_buffer=chunk,
                                        input=True)
        try:
            while True:
                timer = time.time() + 1  # set a timer of 1 second
                # read audio from input stream
                data = self.input_stream.read(chunk)
                data_chunk = array('h', data)
                vol = max(data_chunk)  # get the "loudest" value of the audio
                if(vol >= 2500) and send_aud:  # check if someone is actually talking and not muted
                    while time.time() < timer:  # stream audio for 1 second before checking again for sound
                        data1 = self.input_stream.read(chunk)
                        # convert audio stream to sendable bytestream
                        data2 = base64.b64encode(data1)
                        self.client.publish(
                            f"group5/{self.controller.room_name}/audio/{self.username}", data2)  # sending audio
                else:
                    pass

        except Exception as e:
            self.input_stream.stop_stream()  # stop the input stream
            self.input_stream.close()  # close the input stream
            print(e)
        self.input_stream.close()

    def play_audio(self, audio):
        """
        Send the received audio through the output stream.
        """

        self.output_stream.write(audio)


class GUI(tk.Frame):
    """
    Component to show the GUI of the video feeds.
    """

    def __init__(self, parent, controller, stm):
        self.start_gui(parent, controller, stm)

    def start_gui(self, parent, controller, stm):
        """
        Set up the gui, make a window, make placeholder frames for the video feeds, and show the received videos.
        """

        tk.Frame.__init__(self, parent, bg="#FFFFFF")
        self.stm = stm
        self.controller = controller

        self.controller.grid_rowconfigure(0, weight=1)
        self.controller.grid_columnconfigure(0, weight=1)

        self.controller.title("In Room")

        # self.root = tk.Tk()
        # self.root.title("Virtual Coffee Machine")
        # it freezes when you try to resize the window, so disable resize
        # self.root.resizable(width=False, height=False)
        self.controller.configure(bg="#FFFFFF")
        # self.app.grid()

        # self.root.mainloop()
        # sys.exit(0)

    def run_videochat(self):

        # send video in its own thread
        t1 = Thread(target=self.controller.chat.send_video)
        # send audio in its own thread
        t2 = Thread(target=self.controller.chat.send_audio)
        # start the threads
        t1.start()
        t2.start()

        self.make_spaces()
        self.video_streams()

    def make_spaces(self):
        """
        Make placeholder variables for the video and title for all of the users. 
        Maximum users right now is eight, two office locations and six remote users.
        These are not shown on the GUI unless the respective user actually connects.
        """

        # self.title1 = tk.Label(self.app, text="Offices", font=("Arial Bold", 20), fg="white", bg="#202020")
        #self.title1.grid(row=2, columnspan = 3)
        # self.title2 = tk.Label(self.app, text="Remote Workers", font=("Arial Bold", 20), fg="white", bg="#202020")
        #self.title2.grid(row=5, columnspan = 3)

        # office location 1
        # make a label where the video will be placed

        self.image_image_2 = tk.PhotoImage(
            file=relative_to_images("image_2.png"))

        image = tk.Label(self.controller, image=self.image_image_2)
        image.image = self.image_image_2
        image.grid(row=5)

        self.coffee1 = tk.Label(self)
        self.coffee1.grid(columnspan=2, column=0, row=4, padx=5,
                          pady=5)  # place the label on the grid
        # make a label where the title (username) of the video will be placed
        self.c_title1 = tk.Label(self)
        # place the title on the grid
        self.c_title1.grid(columnspan=2, column=0, row=3, padx=10, pady=10)

        # office location 2
        self.coffee2 = tk.Label(self)
        self.coffee2.grid(columnspan=2, column=1, row=4, padx=5, pady=5)
        self.c_title2 = tk.Label(self)
        self.c_title2.grid(columnspan=2, column=1, row=3, padx=10, pady=10)

        # user 1
        self.user1 = tk.Label(self)
        self.user1.grid(column=0, row=6, padx=5, pady=5)
        self.u_title1 = tk.Label(self)
        self.u_title1.grid(column=0, row=6, padx=10, pady=10, sticky='NE')

        # user 2
        self.user2 = tk.Label(self)
        self.user2.grid(column=1, row=6, padx=5, pady=5)
        self.u_title2 = tk.Label(self)
        self.u_title2.grid(column=1, row=6, padx=10, pady=10, sticky='NE')

        # user 3
        self.user3 = tk.Label(self)
        self.user3.grid(column=2, row=6, padx=5, pady=5)
        self.u_title3 = tk.Label(self)
        self.u_title3.grid(column=2, row=6, padx=10, pady=10, sticky='NE')

        # user 4
        self.user4 = tk.Label(self)
        self.user4.grid(column=0, row=7, padx=5, pady=5)
        self.u_title4 = tk.Label(self)
        self.u_title4.grid(column=0, row=7, padx=10, pady=10, sticky='NE')

        # user 5
        self.user5 = tk.Label(self)
        self.user5.grid(column=1, row=7, padx=5, pady=5)
        self.u_title5 = tk.Label(self)
        self.u_title5.grid(column=1, row=7, padx=10, pady=10, sticky='NE')

        # user 6
        self.user6 = tk.Label(self)
        self.user6.grid(column=2, row=7, padx=5, pady=5)
        self.u_title6 = tk.Label(self)
        self.u_title6.grid(column=2, row=7, padx=10, pady=10, sticky='NE')

        # images for video button
        self.video_on = tk.PhotoImage(file=relative_to_images("video_on.png"))
        self.video_off = tk.PhotoImage(
            file=relative_to_images("video_off.png"))

        # button for toggling video
        self.video_btn = tk.Button(
            self, image=self.video_on, command=lambda: self.stm.send('press_camera'), highlightbackground="#FFFFFF")
        self.video_btn.grid(column=0, row=0, sticky="NW", padx=5, pady=5)

        # images for mute button
        self.muted = tk.PhotoImage(file=relative_to_images("muted.png"))
        self.unmuted = tk.PhotoImage(file=relative_to_images("unmuted.png"))

        # button for toggling audio
        self.audio_btn = tk.Button(
            self, image=self.unmuted, command=lambda: self.stm.send('press_mute'), highlightbackground="#FFFFFF")
        self.audio_btn.grid(column=1, row=0, sticky="NW", padx=5, pady=5)

        label = tk.Label(self, text=self.controller.room_name,
                         font=self.controller.title_font, background="#FFFFFF", fg="#202020")
        label.grid(row=0, column=2)

        self.leave_btn = tk.Button(
            self, text="Leave room", command=lambda: self.stm.send('leaving_room'), highlightbackground="#FFFFFF", fg="#202020")
        self.leave_btn.grid(column=3, row=0, sticky="NW", padx=5, pady=5)

        # this doesn't work yet
        #self.btn = tk.Button(self.app, text="exit", command= self.destroy_window)
        #self.btn.grid(column = 3, row = 0, sticky ="NW", padx = 5, pady = 5)

        # make lists with video and title variables
        self.all_users = [self.coffee1, self.coffee2, self.user1,
                          self.user2, self.user3, self.user4, self.user5, self.user6]
        self.all_titles = [self.c_title1, self.c_title2, self.u_title1,
                           self.u_title2, self.u_title3, self.u_title4, self.u_title5, self.u_title6]

    # def destroy_window(self):
    #     self.root.destroy()

    def disconnect(self):
        """
        Method to disconnect from the broker.
        """

        self.controller.chat.disconnect()

    def video_streams(self):
        """
        Continuously grab the current frame of each user, and show it on the GUI.
        Show the name of each sender next to the video.
        """

        # loop through every user
        for i, user in enumerate(users):
            # get the resolution of the frame
            resolution = images[user].getbbox()
            # if the user isn't connected, don't show video or name
            # if the resolution is 400x300, the user isn't connected to the system (still has placeholder image)
            if resolution[2] == 400 and resolution[3] == 300:
                self.all_users[i].config(bg="#FFFFFF")
                self.all_titles[i].config(bg="#FFFFFF")
            # if the user is connected, show video and name
            else:
                self.all_users[i].config(bg="#202020")
                self.all_titles[i].config(text=f"{user}".capitalize(), font=(
                    "Arial Bold", 15), fg="white", bg="#202020")  # show name
                # convert from Image to ImageTk
                imgtk = ImageTk.PhotoImage(image=images[user])
                # update the user's video in the gui
                self.all_users[i].imgtk = imgtk
                self.all_users[i].configure(image=imgtk)
        # run the method recursively to continue to update every frame
        self.coffee1.after(1, self.video_streams)

    def toggle_video(self):
        """
        Method to toggle the global video variable by button press.
        When it is toggled, the VideoChat object knows when to send video.
        """

        global send_vid
        send_vid = not send_vid  # toggle video
        # change button text
        if send_vid:
            self.video_btn.config(image=self.video_on)
        else:
            self.video_btn.config(image=self.video_off)

    def toggle_audio(self):
        """
        Method to toggle the global audio variable by button press.
        When it is toggled, the VideoChat object knows when to send audio.
        """

        global send_aud
        send_aud = not send_aud  # toggle audio
        # change button text
        if send_aud:
            self.audio_btn.config(image=self.unmuted)
        else:
            self.audio_btn.config(image=self.muted)
