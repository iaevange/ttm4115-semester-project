import tkinter as tk                # python 3
from tkinter import font as tkfont

from cv2 import log  # python 3
from frames.login.build.gui import Login
from frames.roommenu.build.gui import RoomMenu
from frames.joiningroom.build.gui import JoiningRoom
from frames.inroom.videochat import GUI
import time


class App(tk.Tk):

    def __init__(self, stm):
        tk.Tk.__init__(self)

        self.title_font = tkfont.Font(
            family='Helvetica', size=18, weight="bold", slant="italic")
        self.stm = stm
        self.username = None
        self.room_name = None

        # the container is where we'll stack a bunch of frames
        # on top of each other, then the one we want visible
        # will be raised above the others
        container = tk.Frame(self, bg="#FFFFFF")
        container.grid_rowconfigure(0, weight=1)
        container.grid_columnconfigure(0, weight=1)
        self.state('zoomed')
        container.grid(row=0, column=0)

        self.frame = None
        self.chat = None

        self.frames = {}
        for F in (Login, RoomMenu, JoiningRoom, GUI):
            page_name = F.__name__
            frame = F(parent=container, controller=self, stm=self.stm)
            self.frames[page_name] = frame

            # put all of the pages in the same location;
            # the one on the top of the stacking order
            # will be the one that is visible.
            frame.grid(row=0, column=0, sticky="nsew")

        self.show_frame("Login")

    def show_frame(self, page_name, show=False):
        """
        Show a frame for the given page name
        """

        self.frame = self.frames[page_name]
        self.frame.tkraise()
        if show:
            self.frame.show_frame()

    def show_error_message(self):
        """
        Show error message for frame
        """

        self.frame.show_error_message()
        time.sleep(3)

    def toggle_audio(self):
        """
        Toggle audio
        """

        self.frame.toggle_audio()

    def toggle_video(self):
        """
        Toggle video
        """

        self.frame.toggle_video()

    def run_videochat(self):
        """
        Start videochat
        """

        self.frame.run_videochat()

    def connect_to_room(self):
        """
        Connect to room
        """

        self.frame.connect_to_room()

    def disconnect(self):
        """
        Disconnect from room
        """
        self.room_name = None
        self.frame.disconnect()
        self.chat = None

    def sleep(self):
        """
        Sleep for frame
        """

        time.sleep(5)

    def hide_frame(self):
        """
        Hide frame
        """

        self.frame.hide_frame()

    def turn_on_video(self):
        """
        Turn on video
        """

        self.chat.turn_on_video()

    def turn_on_audio(self):
        """
        Turn on audio
        """

        self.chat.turn_on_audio()
